import { Component } from '@angular/core';
import {HeaderComponent} from './header.component';
import {RecipeComponent} from "./recipe/recipe.component";
import {ShoppingListComponent} from "./shopping-list/shopping-list.component";
import {RecipeService} from "./recipe/recipe.service";

@Component({
  moduleId: module.id,
  selector: 'recipe-book-app',
  templateUrl: 'recipe-book.component.html',
  directives:[HeaderComponent, RecipeComponent, ShoppingListComponent],
  providers:[RecipeService]

})
export class RecipeBookAppComponent {
}
