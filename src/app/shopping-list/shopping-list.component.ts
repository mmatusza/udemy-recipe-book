import { Component, OnInit } from '@angular/core';
import {ShoppingListAddComponent} from "./shopping-list-add.component";
import {Ingridient} from "../shared/ingridient";
import {ShoppingListService} from "./shopping-list.service";

@Component({
  moduleId: module.id,
  selector: 'rb-shopping-list',
  templateUrl: 'shopping-list.component.html',
  directives:[ShoppingListAddComponent]
})
export class ShoppingListComponent implements OnInit {

  public ingridients:Ingridient[] = [];
  constructor(private shoppingListSrv:ShoppingListService) {}

  ngOnInit() {
    this.ingridients = this.shoppingListSrv.getIngrigients();
    console.debug("ShoppingListComponent data retrieved")
  }

}
