import {Injectable, OnInit} from '@angular/core';
import {Ingridient} from "../shared/ingridient";

@Injectable()
export class ShoppingListService implements OnInit{
  ngOnInit():any {
    this.ingridients.push(new Ingridient("Chicken", 1, "item"));
    this.ingridients.push(new Ingridient("Potatos", 1, "kg"));
    this.ingridients.push(new Ingridient("Onion", 0.5, "kg"));
    this.ingridients.push(new Ingridient("flour", 1, "cup"));
    console.debug("ShoppingListService initial data added");
    return undefined;
  }

  private ingridients:Ingridient[] = [];

  constructor() {
    console.debug("ShoppingListService created");
  }

  getIngrigients(){
    return this.ingridients;
  }

  addIngridient(ingridient:Ingridient){
    this.ingridients.push(ingridient);
  }

  addIngridients(ingridients:Ingridient[]){
    Array.prototype.push.apply(this.ingridients, ingridients);
  }
}
