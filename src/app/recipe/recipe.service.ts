import {Injectable, EventEmitter} from '@angular/core';
import {Recipe} from "./recipe";
import {Ingridient} from "../shared/ingridient";


@Injectable()
export class RecipeService{

  public evtRecipeSelected = new EventEmitter<Recipe>();


  recipes:Recipe[] = [
    new Recipe( 'Rost', "Sunday Rost", "http://www.royaloakyork.co.uk/wp-content/uploads/2015/11/Sunday-Lunch-Image.jpg", [
      new Ingridient("beef", 750,"g"),
      new Ingridient("flower", 10,"g"),
      new Ingridient("rosmarry", 1,"bunch")
    ]),
    new Recipe( 'Gulasz', "Ummm", "https://realfood.tesco.com/media/images/fa721426-a7c1-4790-afb5-5c1f2f9dc9d0_full.jpg", [])

  ];

  constructor() {
  }


  getRecipes():Recipe[]{
    return this.recipes;
  }

  selectRecipe(recipe:Recipe) {
    this.evtRecipeSelected.emit(recipe);
  }
}
