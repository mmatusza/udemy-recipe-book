import { Component, OnInit } from '@angular/core';

import {RecipeListComponent} from './recipe-list'
import {RecipeDetailsComponent} from "./recipe-details/recipe-details.component";
import {Recipe} from "./recipe";

@Component({
  moduleId: module.id,
  selector: 'rb-recipe',
  templateUrl: 'recipe.component.html',
  directives:[RecipeListComponent, RecipeDetailsComponent]

})
export class RecipeComponent implements OnInit {
  constructor() {}

  ngOnInit() {
  }

}
