import {Component, OnInit, Input} from '@angular/core';
import {Recipe} from "../recipe";
import {RecipeService} from "../recipe.service";
import {ShoppingListService} from "../../shopping-list/shopping-list.service";
import {Ingridient} from "../../shared/ingridient";

@Component({
  moduleId: module.id,
  selector: 'rb-recipe-details',
  templateUrl: 'recipe-details.component.html',
})
export class RecipeDetailsComponent implements OnInit {

  public selectedRecipe:Recipe;
  constructor(private recipeSrv:RecipeService, private shoppingListSrv:ShoppingListService) {
  }

  ngOnInit() {
    this.recipeSrv.evtRecipeSelected.subscribe(rcp =>{
      this.selectedRecipe = rcp;
    });
  }

  addIngridients(ingridients:Ingridient[]){
    this.shoppingListSrv.addIngridients(ingridients);
  }
}
