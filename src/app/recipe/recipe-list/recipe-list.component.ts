import {Component, OnInit, Output, EventEmitter}from '@angular/core';
import {RecipeItemComponent} from "./recipe-item.component";
import {Recipe} from "../recipe";
import {RecipeService} from "../recipe.service";

@Component({
  moduleId: module.id,
  selector: 'rb-recipe-list',
  templateUrl: 'recipe-list.component.html',
  directives: [RecipeItemComponent],

})
export class RecipeListComponent implements OnInit{



  recipes:Recipe[] = [];

  constructor(private recipeSrv:RecipeService){

  }
  ngOnInit() {
    this.recipes = this.recipeSrv.getRecipes();
  }

  onSelectRecipe(recipe:Recipe){
    this.recipeSrv.selectRecipe(recipe);
  }
}
